# Getting Started with React (Typescript)


Install `create-react-app`:

    npm i -g create-react-app


Create a React app:

    create-react-app my-first-app --scripts-version=react-scripts-ts
    cd my-first app

Run testd:

    npm run test


Run in a local dev env:

    npm run start

Deploy:

    npm run build




_tbd_



## References

* [Getting started](https://reactjs.org/docs/hello-world.html)
* [facebookincubator/create-react-app](https://github.com/facebookincubator/create-react-app)
* [Microsoft/TypeScript-React-Starter](https://github.com/Microsoft/TypeScript-React-Starter)
* [Using React in VS Code](https://code.visualstudio.com/docs/nodejs/reactjs-tutorial)
* []()
* [A window.fetch JavaScript polyfill](https://github.com/github/fetch)
* [zeit/next.js](https://github.com/zeit/next.js/)
* [Material-UI](http://www.material-ui.com/)
* []()
* [Higher-Order Components](https://reactjs.org/docs/higher-order-components.html)
* [Ten minute introduction to MobX and React](https://mobx.js.org/getting-started.html)
* [Redux Basics](http://redux.js.org/docs/basics/)
* [Introducing JSX](https://reactjs.org/docs/introducing-jsx.html)
* [Flow](https://flow.org/)
* []()
* []()






